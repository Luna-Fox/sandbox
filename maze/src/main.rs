use rand::Rng;

fn main() {
    let x = 5;
    let y = 5;
    let maze = mazegen(x, y);
    let out: Vec<char> = maze.into_iter().map(mazemap).collect();
    for i in 0..(x*y) {
        if i % x == 0 {
            println!();
        }
        print!("{}", out[i]);
    }
    
}

fn mazegen(x: usize, y: usize) -> Vec<bool> {
    let mut maze = Vec::new();
    let mut rng = rand::thread_rng();
    for _ in 0..(x*y) {
        maze.push(rng.gen::<bool>());
    }
    return maze;

}

fn mazemap(outp: bool) -> char {
    if outp == true {
        return '.';
    } else {
        return '#';
    }
}